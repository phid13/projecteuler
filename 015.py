modulo = int(10e8 + 7)

import math
t = int(input())
for _ in range(t):
    n, m = input().strip().split(' ')
    n, m = [int(n), int(m)]
    answer = 1
    for i in range(m):
        answer *= (n + i + 1)
        answer //= (i + 1)
    answer %= modulo
    print(int(answer))

