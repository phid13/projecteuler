import math
import bisect

max_n = 5000

def gen_fib_digits(max_n):
    n = 0
    fib_digits = [1, 1]
    f1 = 1
    f2 = 1
    while n < max_n:
        new_f = f1 + f2
        f1 = f2
        f2 = new_f
        cur_length = math.floor(math.log10(f2)) + 1
        if cur_length > n:
            n = cur_length
        fib_digits.append(n)
    return fib_digits

fib_digits = gen_fib_digits(max_n)

t = int(input())
for _ in range(t):
    n = int(input())
    print(bisect.bisect_left(fib_digits, n) + 1)
