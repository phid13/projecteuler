t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    primes = []
    for i in range(2, n+1):
        is_prime = True
        for prime in primes:
            if i % prime == 0:
                is_prime = False
        if is_prime:
            primes.append(i)
    output = 1
    for prime in primes:
        power = 1
        while prime ** power <= n:
            power += 1
        power -= 1
        output = output * (prime ** power)
    print(output)

            
        

