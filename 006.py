t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    print(int(((n*(n+1)>>1) ** 2) - (n * (n+1) * (2*n+1) / 6)))

