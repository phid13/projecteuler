threshold = 28124 # all numbers higher or equal are yes

def sum_factors(num):
    sum_out = 1
    i = 2
    while i * i < num:
        if num % i == 0:
            sum_out += i
            sum_out += num // i
        i += 1
    if i * i == num:
        sum_out += i
    return sum_out

def gen_abundant(max):
    abundant = []
    for i in range(10, max):
        if sum_factors(i) > i:
            abundant.append(i)
    return abundant

abundant = gen_abundant(threshold)

t = int(input())
for _ in range(t):
    n = int(input())
    output = "NO"
    if n >= threshold:
        output = "YES"
    else:
        for val in abundant[:(len(abundant) // 2 + 1)]:
            if (n - val) in abundant:
                output = "YES"
    print(output)
