word = 'abcdefghijklm'

max_n = 13

def gen_factorials(max_n):
    factorials = []
    cur_factorial = 1
    for i in range(max_n - 1):
        num = i + 1
        cur_factorial *= num
        factorials.append(cur_factorial)
    return factorials[::-1]

factorials = gen_factorials(max_n)

t = int(input())
for _ in range(t):
    n = int(input()) - 1
    letters = list(word)
    i = 0 
    output = []
    while i < max_n - 1:
        cur_letter = n // factorials[i]
        n %= factorials[i]
        output.append(letters.pop(cur_letter))
        i += 1
    output.append(letters[0])
    print(''.join(output))
