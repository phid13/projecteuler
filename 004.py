t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    a = 0
    i = n - 1
    while i >= 100:
        if str(i) == str(i)[::-1]:
            j = 999
            while j > 100:
                if i % j == 0:
                    if (i / j) < 1000:
                        a = i
                j = j - 1
                if a != 0:
                    break
        if a != 0:
            break
        i = i - 1
    print(a)
