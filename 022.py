def value(query):
    value = 0
    for char in query:
        value += ord(char) - 96
    return value

n = int(input())
names = []
for _ in range(n):
    names.append(input().lower())
names.sort()

q = int(input())
for _ in range(q):
    query = input().lower()
    print(value(query) * (names.index(query) + 1))
