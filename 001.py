t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    n -= 1
    a = int(n/3)
    b = int(n/5)
    c = int(n/15)
    A = 3 * (a * (a + 1))
    B = 5 * (b * (b + 1))
    C = 15 * (c * (c + 1))
    total = (A + B - C) >> 1
    print(int(total))

