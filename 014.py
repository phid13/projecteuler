def collatz(val):
    if val%2 == 0:
        val = int(val / 2)
    else:
        val = 3 * val + 1
    return val

max_n = int(5 * 10e5)

lengths = [0] * (int(max_n) - 1)
lengths.append(1) # length to get to 1

max_val = max_n
for i in range(int(max_n), 0, -1):
    cur_length = 0
    val = i
    while lengths[-val] == 0:
        val = collatz(val)
        cur_length += 1
        if val > max_val:
            while val > max_val:
                val = collatz(val)
                cur_length += 1
    lengths[-i] = cur_length + lengths[-val]

maximums = []
highest = 0
index = 0
for i, val in enumerate(lengths[::-1]):
    if val >= highest:
        highest = val
        index = i + 1
    maximums.append(index)

t = int(input())
for _ in range(t):
    n = int(input())
    print(maximums[n - 1])
