t = int(input())
for _ in range(t):
    n = int(input())
    digits = str(2 ** n)
    output = 0
    for char in digits:
        output += int(char)
    print(output)
