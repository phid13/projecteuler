def horizontal(grid):
    my_prod = 0
    for row in grid:
        for col in range(len(row) - 3):
            a = row[col]
            b = row[col + 1]
            c = row[col + 2]
            d = row[col + 3]
            cur_prod = a * b * c * d
            my_prod = max(my_prod, cur_prod)
    return my_prod
        
def vertical(grid):
    grid_t = [[grid[j][i] for j in range(len(grid))] for i in range(len(grid[0]))]
    my_prod = 0
    for row in grid_t:
        for col in range(len(row) - 3):
            a = row[col]
            b = row[col + 1]
            c = row[col + 2]
            d = row[col + 3]
            cur_prod = a * b * c * d
            my_prod = max(my_prod, cur_prod)
    return my_prod

def down_right_diag(grid):
    my_prod = 0
    for row in range(len(grid) - 3):
        for col in range(len(grid[0]) - 3):
            a = grid[row][col]
            b = grid[row + 1][col + 1]
            c = grid[row + 2][col + 2]
            d = grid[row + 3][col + 3]
            cur_prod = a * b * c * d
            my_prod = max(my_prod, cur_prod)
    return my_prod

def up_right_diag(grid):
    my_prod = 0
    for row in range(3, len(grid)):
        for col in range(len(grid[0]) - 3):
            a = grid[row][col]
            b = grid[row - 1][col + 1]
            c = grid[row - 2][col + 2]
            d = grid[row - 3][col + 3]
            cur_prod = a * b * c * d
            my_prod = max(my_prod, cur_prod)
    return my_prod

def Main():
    grid = []
    for grid_i in range(20):
        grid_t = [int(grid_temp) for grid_temp in input().strip().split(' ')]
        grid.append(grid_t)
    print(max(horizontal(grid), vertical(grid), down_right_diag(grid), up_right_diag(grid)))

Main()
