t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    i = 2
    while i * i <= n:
        while n % i == 0:
            if i != n:
                n = n / i
            else:
                break
        i = i + 1
    print(int(n))
