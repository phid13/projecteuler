vals = []
for i in range(3000):
    vals.append(-1)

for n in range(1, 55):
    for m in range(1, n):
        a = n**2 - m**2
        b = 2*n*m
        c = n**2 + m**2
        idx = a + b + c

        my_idx = idx - 1
        i = 1
        while my_idx < 3000:
            if vals[my_idx] < a*b*c*(i**3):
                vals[my_idx] = a*b*c*(i**3)
            i += 1
            my_idx = idx * i - 1

t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    print(vals[n-1])
