t = int(input())
for _ in range(t):
    n = int(input())
    fact = 1
    for val in range(2, n+1):
        fact *= val
    str_fact = str(fact)
    output = 0
    for char in str(fact):
        output += int(char)
    print(output)
