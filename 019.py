from datetime import date, timedelta

t = int(input())
for _ in range(t):
    start = list(map(int, input().split()))
    end = list(map(int, input().split()))
    s_year, s_month, s_day = start[0], start[1], start[2]
    e_year, e_month, e_day = end[0], end[1], end[2]
    if s_day != 1:
        if s_month == 12:
            s_month = 1
            s_year += 1
        else:
            s_month += 1

    year_diff = e_year - s_year
    s_year %= 2800
    s_year += 2800
    e_year = s_year + year_diff

    s_date = date(s_year, s_month, 1)
    e_date = date(e_year, e_month, 1)

    cur_date = date(s_year, s_month, 1)
    cur_year = s_year
    cur_month = s_month
    sundays = 0
    while cur_date <= e_date:
        if (cur_date.weekday() == 6):
            sundays += 1
        if cur_month == 12:
            cur_month = 1
            cur_year += 1
        else:
            cur_month += 1
        cur_date = date(cur_year, cur_month, 1)
    print(sundays)

