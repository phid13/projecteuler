from bisect import bisect

triangles = [1]
num_of_factors = [1]

def next_triangle(cur, adder, most):
    cur += adder
    adder += 1
    factors = 0
    i = 1
    while i * i < cur:
        if cur % i == 0:
            factors += 2
        i += 1
    if i * i == cur:
        factors += 1
    if factors > most:
        most = factors
        triangles.append(cur)
        num_of_factors.append(factors)
    return cur, adder, most

cur, adder, most = next_triangle(1, 2, 0)
while num_of_factors[-1] <= int(10e2):
    cur, adder, most = next_triangle(cur, adder, most)

t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    idx = bisect(num_of_factors, n)
    print(triangles[idx])
