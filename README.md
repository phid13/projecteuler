These are my solutions to the project euler + problems posted on hackerrank.

Some solutions use code from other people's solutions, however it is usually not possible for me to entirely copy somebody elses solution others aren't doing the problems on hackerrank which has stricter requirements for solutions.

