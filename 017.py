import math

digits = ['Zero', 'One', 'Two', 'Three', 'Four', 'Five', 'Six', 'Seven', 'Eight', 'Nine']
teens = ['Ten', 'Eleven', 'Twelve', 'Thirteen', 'Fourteen', 'Fifteen', 'Sixteen', 'Seventeen', 'Eighteen', 'Nineteen', 'Twenty']
tens = ['Twenty', 'Thirty', 'Forty', 'Fifty', 'Sixty', 'Seventy', 'Eighty', 'Ninety']
higher = ['', 'Thousand', 'Million', 'Billion', 'Trillion']

def three_digits(one, two, three):
    string = []
    if int(one) != 0:
        string.append(digits[int(one)])
        string.append('Hundred')
    if (10 * int(two) + int(three)) >= 20:
        string.append(tens[int(two) - 2])
        if int(three) != 0:
            string.append(digits[int(three)])
    elif (10 * int(two) + int(three)) >= 10:
        string.append(teens[int(three)])
    else:
        if int(three) != 0:
            string.append(digits[int(three)])
    return string

t = int(input())
for _ in range(t):
    n = int(input())
    if n == 0:
        print('Zero')
    else:
        str_n = str(n)
        output = []
        split = []
        count = 3
        for i in range(len(str_n) - 1, -1, -1):
            if count == 3:
                count = 0
                split = [[]] + split
            split[0] = [str_n[i]] + split[0]
            count += 1
        while len(split) > 0:
            cur = split.pop(0)
            while len(cur) < 3:
                cur = [0] + cur
            three = three_digits(cur[0], cur[1], cur[2])
            if (three != ['Zero']) & (three != []):
                output = output + three
                output.append(higher[len(split)])
        print(' '.join(output))

