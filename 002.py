t = int(input().strip())
for a0 in range(t):
    n = int(input().strip())
    a = 2
    b = 3
    c = 0
    total = 0
    while a <= n:
        if a % 2 == 0:
            total = total + a
        c = a
        a = b
        b += c
    print(total)
        
