t = int(input().strip())
for a0 in range(t):
    n,k = input().strip().split(' ')
    n,k = [int(n),int(k)]
    num = input().strip()
    highest = 0
    for i in range(n - k + 1):
        small = num[i:i+k]
        total = 1
        for val in small:
            total = total * int(val)
        if total > highest:
            highest = total
    print(highest)
