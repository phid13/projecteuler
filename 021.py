max_n = int(10e4)

def sum_divisors(x):
    i = 2
    divisor_sum = 1
    while i * i < x:
        if x % i == 0:
            divisor_sum += i
            divisor_sum += (x // i)
        i += 1
    if i * i == x:
        divisor_sum += i
    return divisor_sum

def gen_amicable(n):
    amicable = []
    for i in range(0, n+1):
        amicable.append(sum_divisors(i))
    return amicable

amicable_nums = gen_amicable(max_n)

t = int(input())
for _ in range(t):
    n = int(input())
    output = 0
    for i in range(200, n):
        if amicable_nums[i] < len(amicable_nums):
            if i == amicable_nums[amicable_nums[i]]:
                if i != amicable_nums[i]:
                    output += i
    print(output)
