## Using a* search algorithm

def compute_f(maze, path, rowH):
    f = 0
    for i, row in enumerate(path):
        f += maze[i][row]
    f += rowH[len(path) - 1]
    return f

t = int(input())
for _ in range(t):
    n = int(input())
    maze = []
    for __ in range(n):
        maze.append(list(map(int, input().split())))
    maxH = 0
    for row in maze:
        maxH += max(row)
    rowH = []
    h = maxH
    for row in maze:
        h -= max(row)
        rowH.append(h)

    paths = [[0]]
    cur_best = 0
    
    # terminates when cur_best < f of any path and cur_best has required length
    terminate = False
    while True:
        if terminate:
            break
        else:
            best = 0
            for path in paths:
                f = compute_f(maze, path, rowH)
                if f > best:
                    best = f
            for i, path in enumerate(paths):
                f = compute_f(maze, path, rowH)
                if best == f:
                    if len(path) == len(maze):
                        output = 0
                        for i in range(len(path)):
                            output += maze[i][path[i]]
                        terminate = True
                        break
                    else:
                        paths.pop(i)
                        paths.append(path + [path[-1]])
                        paths.append(path + [path[-1] + 1])
                    break
    print(output)


